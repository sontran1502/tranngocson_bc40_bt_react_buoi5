import * as type from "../constant/constant"
import { dataShoe } from "../dateShoe"

const initialState = {
    listShoe: dataShoe,
    cart: [],
}

export const shoeReducer = (state = initialState, action) => {
    switch (action.type) {
        case type.ADD_TO_CART: {
            let cloneCart = [...state.cart];

            let index = cloneCart.findIndex((item) => {
                return item.id == action.payload.id
            })
            if (index == -1) {
                let newShoe = { ...action.payload, soluong: 1 };
                cloneCart.push(newShoe);
            } else {
                cloneCart[index].soluong += 1;
            }
            // console.log("cart",state.cart)
            return { ...state, cart: cloneCart }
        }
        case type.DELETE_ITEM:{
            let newCart= state.cart.filter((item) => { 
                return item.id!=action.payload
             })
             return{...state,cart:newCart};
        }
        case type.COUNT_UP_DOWN:{
            let cloneCart = [...state.cart];

            let index = cloneCart.findIndex((item) => { 
                return item.id == action.payload.id
             })

            if(cloneCart[index].soluong==1&&action.payload.num==1){
                cloneCart[index].soluong+=action.payload.num
            }else if(cloneCart[index].soluong==1&&action.payload.num==-1){
                cloneCart.splice(index,1);
            }else if(cloneCart[index].soluong>1){
                cloneCart[index].soluong+=action.payload.num
            }else{
                cloneCart.splice(index,1);
            }

            return{...state,cart:cloneCart};
        }
        default:
            return state
    }
}
