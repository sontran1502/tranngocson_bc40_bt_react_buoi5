import React, { Component } from 'react'
import Cart from './Cart'
import ListShoe from './ListShoe'

export default class ShoePage extends Component {
  render() {
    return (
      <div className='container'>
        <h2 className='text-center'>Shoe Shop</h2>
        <Cart/>
        <ListShoe/>
      </div>
    )
  }
}
