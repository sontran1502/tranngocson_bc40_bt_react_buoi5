import React, { Component } from 'react'
import { connect } from 'react-redux'
import ItemShoe from './ItemShoe'

 class ListShoe extends Component {
    renderItem= () => { 
      return this.props.listShoe.map((item) => { 
        return <ItemShoe item={item}/>
       })
     }
  render() {
    return (
      <div className='row'>
        {this.renderItem()}
      </div>
    )
  }
}
let mapStateToProps =(state) => { 
    return{
        listShoe: state.shoeReducer.listShoe,
    }

 }

export default connect (mapStateToProps)(ListShoe)