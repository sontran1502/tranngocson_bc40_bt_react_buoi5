import React, { Component } from 'react'
import { connect } from 'react-redux';
import { ADD_TO_CART } from './constant/constant';

 class ItemShoe extends Component {
    render() {
        let { image, name, price } = this.props.item;
        return (
            <div className='col-md-3 p-4 '>
                <div className=" card" >
                    <img src={image} className="card-img-top" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">{name}</h5>
                        <h3 className="card-text ">Giá: {price} $</h3>
                        <a href="#" onClick={() => { this.props.handlePushToCart(this.props.item) }}  className="btn btn-primary mb-3">Add To Card</a>
                    </div>
                </div>
            </div>
        )
    }
}

let mapDispatchToProps= (dispatch)=>{
    return{
        handlePushToCart: (shoe) => { 
            let action={
                type: ADD_TO_CART,
                payload:shoe
            };
            dispatch(action);
         }
    }
}

export default connect (null,mapDispatchToProps)(ItemShoe)