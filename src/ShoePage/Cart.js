import React, { Component } from 'react'
import { connect } from 'react-redux'
import { COUNT_UP_DOWN, DELETE_ITEM } from './constant/constant'

class Cart extends Component {
    renderTbody = () => {

        return this.props.cart.map((item) => {
            return (
                <tr>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>
                        <button onClick={() => {
                            this.props.hadleUpAndDown(item.id, -1)
                        }} className='btn btn-danger'>-</button>
                        <strong className='mx-3'>{item.soluong}</strong>
                        <button onClick={() => {
                            this.props.hadleUpAndDown(item.id, 1)
                        }} className='btn btn-success'>+</button>
                    </td>
                    <td>
                        {item.price * item.soluong} $
                    </td>
                    <td>
                        <img src={item.image} style={{ width: 50 }} alt="" />
                    </td>
                    <td>
                        <button
                            onClick={() => { this.props.handleDelete(item.id) }}
                            className='btn btn-danger'>Delete
                        </button>
                    </td>
                </tr>
            )
        })
    }
    render() {
        return (
            <div>
                <table className='table'>
                    <thead>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Img</th>
                        <th>Action</th>
                    </thead>
                    <tbody >
                        {this.renderTbody()}
                    </tbody>
                </table>
            </div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        cart: state.shoeReducer.cart
    }
}
let mapDispatchToProps = (dispatch) => {
    return {
        handleDelete: (id) => {
            let action = {
                type: DELETE_ITEM,
                payload: id,
            }
            dispatch(action);
        },
        hadleUpAndDown: (id, num) => {
            let action = {
                type: COUNT_UP_DOWN,
                payload: { id, num },
            }
            dispatch(action);
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Cart)